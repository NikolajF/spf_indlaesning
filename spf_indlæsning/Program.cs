﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using LE34.Data.Access;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Reflection;

namespace spf_indlæsning
{
	class Program
	{
        // her læses fra live data på ftp-server
        //		static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
        static string path = "";


        static void Main(string[] args)
		{
            path= ConfigurationManager.AppSettings["mappe"];
            Console.WriteLine(path);
            int række = 0;		
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
            bool force = false;
            if (args.Length == 1 && args[0] == "force")
                force = true;
            // læs csv-fil - den er ikke større end at den blot indlæses i et hug
            // filen skal findes i afviklingsmappen - der gemmer ftp vistnok
            // filen skal være nyere end 1 time
            string filnavn = path + "webgis.csv";
			DateTime filtid = File.GetLastWriteTime(filnavn);
			if (DateTime.Compare(filtid, DateTime.Now.AddDays(-1)) != 1 && force == false) // fejl
			{
				skrivLog("Ingen nye data at indlæse","");
			}
			else
			{
				skrivLog("Læser webgis.csv", filtid.ToString());
				string[] ind = File.ReadAllLines(filnavn, Encoding.GetEncoding(1252));
                // connection_string findes i app.config
                DataBase dbop = new DataBase(ConfigurationManager.ConnectionStrings["landbrug_foedevarer"].ConnectionString);
                DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["landbrug_foedevarer"].ConnectionString);
				db.BeginTransaction();
				try
				{
					// slet - eller måske senere flyt - eksisterende data fra tabellen spf
					db.SetSQLStatement("truncate spf_svin");
					db.Execute(false);
					// find alle chr-numre med svin
					db.SetSQLStatement("insert into spf_svin(chr_nr) select distinct chr_nr from chr_samlet where dyreart=15");
					db.Execute(false);

					// læs spf_svin ind i datatable, så den kan opdateres / kontrolleres for dubletter
					db.SetSQLStatement("select * from spf_svin");
					db.Query(false);
					db.Result.TableName = "spf_svin";


					// insert til tabellen spf
					foreach (string lin in ind)
					{
                        række++;
                        //Console.WriteLine("skriver : " + reader.Peek());

                        string[] v = lin.Split(';');

						if (v[0] != "CHRNR")
						{
							int tal = int.Parse(v[0]);
							if (tal < 10000 || tal >= 900000)
							{
								// så er det et besætningsnummer, og så skal chr_nr findes i chr_samlet
								tal = dbop.SingleSelect<int>("select chr_nr from chr_samlet where bes_nr= " + tal);
							}
							// find posten i db.result
							DataRow[] r = db.Result.Select("chr_nr=" + tal);
							if (r.Length > 0)
							{
								r[0]["sus"] = v[4];
								r[0]["spf_status"] = v[3];
                                // PRRS 
                                if (v[4].Contains("sanPRRS1") || v[4].Contains("sanPRRS2"))
                                    r[0]["prrs"] = "Sanering";
                                else if (v[4].Contains("PRRS1") || v[4].Contains("PRRS2"))
                                    r[0]["prrs"] = "Positiv";
                                else
                                    r[0]["prrs"] = "Negativ";
                                //if (v[4].Contains("sanDK") || v[4].Contains("sanVac"))
                                //	r[0]["prrs"] = "Sanering";
                                //else if (v[4].Contains("DK") || v[4].Contains("Vac"))
                                //	r[0]["prrs"] = "Positiv";
                                //else 
                                //	r[0]["prrs"] = "Negativ";


                                // ??? vi har ikke fået en liste med prioritering af statusværdier, men ukendt og positiv må i hvert fald have forrang
                                if (r[0]["ap2"].ToString() == "6")
								{
									r[0]["myc"] = v[1];
									r[0]["ap2"] = v[2];
								}
								else
								{
									//Debug.Print(tal.ToString() + ": " + r[0]["myc"].ToString() + " = " + v[1] + " - " + r[0]["ap2"].ToString() + " = " + v[2]);
									// så skal myc og ap2 behandles hver for sig - 4 og 5 gør vi ikke noget ved
									if (int.Parse(r[0]["myc"].ToString()) < 4 && int.Parse(v[1]) > 3)
									{
										r[0]["myc"] = v[1];
									}
									if (int.Parse(r[0]["ap2"].ToString()) < 4 && int.Parse(v[2]) > 3)
									{
										r[0]["ap2"] = v[2];
									}
								}
								r[0].AcceptChanges();
							}
							//db.SetSQLStatement("insert into spf(chr_nr, myc, ap2) values(" + v[0] + "," + v[1] + "," + v[2] + ");");
							//db.Execute(false);

						}
					}
					//db.Result.AcceptChanges();
					foreach (DataRow r in db.Result.Rows)
					{
						if (r["ap2"].ToString() != "6")
						{
							db.SetSQLStatement("update spf_svin set ap2='" + r["ap2"].ToString() + "', myc='" + r["myc"].ToString() + "', sus='" + r["sus"].ToString() + "', spf_status='" + r["spf_status"].ToString() + "', prrs='" + r["prrs"].ToString() + "' where chr_nr=" + r["chr_nr"].ToString());
							db.Execute(false);
						}
					}
					// slut indlæsning


//					db.CommitTransaction();

					// og så beregningerne
//					db.BeginTransaction();
					try
					{
						// beregn først det samlede smittetal for alle poster i spf, uanset om gården er smittet med det ene eller andet - det kontrolleres jo i den efterfølgende opdatering
						db.SetSQLStatement("update spf_svin set smittetal = a.c from (select chr_nr, sum(antal_dyr1) * 0.3 + sum(antal_dyr2) * 0.08 + sum(antal_dyr4) * 0.17 as c from chr_samlet where dyreart=15 group by chr_nr) as a where a.chr_nr=spf_svin.chr_nr;");
						//				db.SetSQLStatement("update spf_svin set smittetal = a.c from (select chr_nr, sum(antal_dyr1) * 0.3 + sum(antal_dyr2) * 0.08 + sum(antal_dyr4) * 0.17 as c from chr_samlet where dyreart=15 group by chr_nr) as a where a.chr_nr=spf_svin.chr_nr and (spf_svin.myc>3 or spf_svin.ap2>3);");
						db.Execute(false);

						// dernæst smitterisiko = summen af naboer divideret med afstanden - først for myc
						// ??? naboerne skal have myc>3, gården selv behøver ikke - det er vist ikke sådan det kører nu (samme ændring skal i så fald laves for ap2)
						db.SetSQLStatement("update spf_svin set myc_risiko = 0.899^(2.14*1.53^log(2,b.r::numeric/0.666))  from (select a.c, sum(a.risiko) as r from (select distinct f.chr_nr as c, t.chr_nr, st_distance(f.geom,t.geom) as afstand, u.smittetal / st_distance(f.geom,t.geom) as risiko from chr_samlet f, chr_samlet t, spf_svin g, spf_svin u where g.myc>3 and u.myc>3 and g.smittetal >0 and u.smittetal >0 and f.chr_nr=g.chr_nr and t.chr_nr=u.chr_nr and st_distance(f.geom,t.geom) <=3000 and st_distance(f.geom,t.geom) >0 and f.chr_nr<>t.chr_nr and f.dyreart =15 and t.dyreart =15) a group by a.c) b where spf_svin.chr_nr=b.c and spf_svin.smittetal >0");
						db.Execute(false);

						// og dernæst smitterisiko for ap2
						db.SetSQLStatement("update spf_svin set ap2_risiko = 0.985^(2.25*1.74^log(2,b.r::numeric/0.666))  from (select a.c, sum(a.risiko) as r from (select distinct f.chr_nr as c, t.chr_nr, st_distance(f.geom,t.geom) as afstand, u.smittetal / st_distance(f.geom,t.geom) as risiko from chr_samlet f, chr_samlet t, spf_svin g, spf_svin u where g.ap2>3 and u.ap2>3 and g.smittetal >0 and u.smittetal >0 and f.chr_nr=g.chr_nr and t.chr_nr=u.chr_nr and st_distance(f.geom,t.geom) <=3000 and st_distance(f.geom,t.geom) >0 and f.chr_nr<>t.chr_nr and f.dyreart =15 and t.dyreart =15) a group by a.c) b where spf_svin.chr_nr=b.c and spf_svin.smittetal >0");
						db.Execute(false);

                        // samlede smittetal for PRRS, de er vægtet anderledes
                        db.SetSQLStatement("update spf_svin set prrs_smittetal = a.c from (select chr_nr, sum(antal_dyr1) * 0.45 + sum(antal_dyr2) * 0.10 + sum(antal_dyr4) * 0.17 as c from chr_samlet where dyreart=15 group by chr_nr) as a where a.chr_nr=spf_svin.chr_nr and spf_svin.prrs in('Ukendt','Positiv');");
                        db.Execute(false);

                        db.CommitTransaction();
						db.Close();
						skrivLog("Indlæsning og beregning af SPF-data udført", "");

					}
					catch (Exception ex)
					{
						Console.Write(ex.Message);
						skrivLog("Beregning af SPF-data fejlede!", ex.Message);
						// man kan ikke rulle tilbage hvis det er timeout
						try 
						{
							db.RollbackTransaction();
							db.Close();
						}
						catch //(Exception ex)
						{
						}
						//throw new Exception("Beregning af SPF-data fejlede!");
					}
				}
				catch (Exception ex)
				{
					Console.Write(ex.Message);
					skrivLog("Insert af SPF-data fejlede i række ", række + "  -  " + ex.Message);
					try
					{
						db.RollbackTransaction();
						db.Close();
					}
					catch //(Exception ex)
					{
					}
//					throw new Exception("Insert af SPF-data fejlede!");

				}
			}
		}

		static void skrivLog(string besked, string system_fejlmeddelelse)
		{
			StreamWriter wr;

			// Open the file directly using StreamWriter. 
			try
			{
				wr = new StreamWriter(path + "indlæs_spf.log", true, System.Text.Encoding.GetEncoding(1252));
			}
			catch //(IOException exc)
			{
				// throw new Exception("Der er sket en fejl i skrivLog:\r\n" + exc.Message);
				return;
			}
			wr.WriteLine(DateTime.Now.ToString() + "\t" + besked + "\t" + system_fejlmeddelelse);
			wr.Close();
		}

//    static void Main_intern_ftp(string[] args)
//    {
//      Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
		
			
//      // hent fil fra ftp-server - adresse og login findes i app.config
//      Stream responseStream=null;
//      try
//      {                

//      // Get the object used to communicate with the server.
//      FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftp.spf.dk/webgis.csv");

//      // This example assumes the FTP site uses anonymous logon.
//      request.Credentials = new NetworkCredential("webgis", "asdrty67");
//      request.Method = WebRequestMethods.Ftp.DownloadFile;
//      request.UsePassive = false;
//      //request.Proxy = null;
//      //request.UseBinary = false;

//      FtpWebResponse response = (FtpWebResponse)request.GetResponse();

//      responseStream = response.GetResponseStream();
//      StreamWriter sr = new StreamWriter("d:\\www\\LandbrugFoedevarer\\HentSPF\\webgis.csv",false, System.Text.Encoding.GetEncoding(1252), 2048);
////				FileStream writeStream = new FileStream(@"d:\www\LandbrugFoedevarer\HentSPF\webgis.csv", FileMode.Create);                
//      int Length = 2048;
//      Byte[] buffer = new Byte[Length];
//      int bytesRead = responseStream.Read(buffer, 0, Length);               
//      while (bytesRead > 0)
//      {
//        sr.Write(buffer);
//          bytesRead = responseStream.Read(buffer, 0, Length);
//      }
//      sr.Close();

//        StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(1252));

//      Console.WriteLine("Status: " + response.StatusDescription);



//      // læs csv-fil - den er ikke større end at den blot indlæses i et hug
//      //string filnavn = @"c:\data\Landbrug og fødevarer\SmitterisikoTilWebgis.csv"; // ??? dette skal ikke være hardcoded, men ligge i app.config
//      //			string[] ind = File.ReadAllLines(filnavn,Encoding.GetEncoding(1252));
//      // connection_string findes i app.config
//      DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["landbrug_foedevarer"].ConnectionString);
//      db.BeginTransaction();
//      try
//      {
//        // slet - eller måske senere flyt - eksisterende data fra tabellen spf
//        db.SetSQLStatement("truncate spf_svin");
//        db.Execute(false);
//        // find alle chr-numre med svin
//        db.SetSQLStatement("insert into spf_svin(chr_nr) select distinct chr_nr from chr_samlet where dyreart=15");
//        db.Execute(false);

//        // læs spf_svin ind i datatable, så den kan opdateres / kontrolleres for dubletter
//        db.SetSQLStatement("select * from spf_svin");
//        db.Query(false);
//        db.Result.TableName = "spf_svin";


//        // insert til tabellen spf
//        while (reader.Peek() >= 0)
//        {
//          //Console.WriteLine("skriver : " + reader.Peek());
//          string[] v = reader.ReadLine().Split(';');

//          if (v[0] != "CHRNR")
//          {
//            int tal = int.Parse(v[0]);
//            if (tal < 10000 || tal >= 900000)
//            {
//              // så er det et besætningsnummer, og så skal chr_nr findes i chr_samlet
//              tal = db.SingleSelect<int>("select chr_nr from chr_samlet where bes_nr= " + tal);
//            }
//            // find posten i db.result
//            DataRow[] r = db.Result.Select("chr_nr=" + tal);
//            if (r.Length > 0)
//            {
//              r[0]["sus"] = v[4];
//              r[0]["spf_status"] = v[3];

//              // ??? vi har ikke fået en liste med prioritering af statusværdier, men ukendt og positiv må i hvert fald have forrang
//              if (r[0]["ap2"].ToString() == "6")
//              {
//                r[0]["myc"] = v[1];
//                r[0]["ap2"] = v[2];
//              }
//              else
//              {
//                //Debug.Print(tal.ToString() + ": " + r[0]["myc"].ToString() + " = " + v[1] + " - " + r[0]["ap2"].ToString() + " = " + v[2]);
//                // så skal myc og ap2 behandles hver for sig - 4 og 5 gør vi ikke noget ved
//                if (int.Parse(r[0]["myc"].ToString()) < 4 && int.Parse(v[1]) > 3)
//                {
//                  r[0]["myc"] = v[1];
//                }
//                if (int.Parse(r[0]["ap2"].ToString()) < 4 && int.Parse(v[2]) > 3)
//                {
//                  r[0]["ap2"] = v[2];
//                }
//              }
//              r[0].AcceptChanges();
//            }
//            //db.SetSQLStatement("insert into spf(chr_nr, myc, ap2) values(" + v[0] + "," + v[1] + "," + v[2] + ");");
//            //db.Execute(false);

//          }
//        }
//        //db.Result.AcceptChanges();
//        foreach (DataRow r in db.Result.Rows)
//        {
//          if (r["ap2"].ToString() != "6")
//          {
//            db.SetSQLStatement("update spf_svin set ap2='" + r["ap2"].ToString() + "', myc='" + r["myc"].ToString() + "', sus='" + r["sus"].ToString() + "', spf_status='" + r["spf_status"].ToString() + "' where chr_nr=" + r["chr_nr"].ToString());
//            db.Execute(false);
//          }
//        }
//      }
//      catch (Exception ex)
//      {
//        Console.Write(ex.Message);
//        db.RollbackTransaction();
//        db.Close();
//        reader.Close();
//        response.Close();
//        throw new Exception("Insert af SPF-data fejlede!");
//      }
//      // skriv at importen er lykkedes i spf_opdatering

//      db.CommitTransaction();
//      db.Close();
//      reader.Close();
//      response.Close();
//      }
//      catch (WebException wEx)
//      {
//        Console.WriteLine("WebException - " + wEx.Message + "\n" + responseStream.ToString());
//      }
//      catch (Exception ex)
//      {
//        Console.WriteLine("Exception - " + ex.Message);
//      }

//    }

	
		
		
		
//    static void Main_gl(string[] args)
//    {
//      Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
//      // hent fil fra ftp-server - adresse og login findes i app.config
			
//      // læs csv-fil - den er ikke større end at den blot indlæses i et hug
//      string filnavn = @"c:\data\Landbrug og fødevarer\SmitterisikoTilWebgis.csv"; // ??? dette skal ikke være hardcoded, men ligge i app.config
//      string[] ind = File.ReadAllLines(filnavn,Encoding.GetEncoding(1252));
//      // connection_string findes i app.config
//      DataBase db = new DataBase(ConfigurationManager.ConnectionStrings["landbrug_foedevarer"].ConnectionString);
//      db.BeginTransaction();
//      try
//      {
//        // slet - eller måske senere flyt - eksisterende data fra tabellen spf
//        db.SetSQLStatement("truncate spf_svin");
//        db.Execute(false);
//        // find alle chr-numre med svin
//        db.SetSQLStatement("insert into spf_svin(chr_nr) select distinct chr_nr from chr_samlet where dyreart=15");
//        db.Execute(false);

//        // læs spf_svin ind i datatable, så den kan opdateres / kontrolleres for dubletter
//        db.SetSQLStatement("select * from spf_svin");
//        db.Query(false);
//        db.Result.TableName = "spf_svin";
//        //DataTable dt = db.Result;


//        // insert til tabellen spf
//        foreach (string lin in ind)
//        {
//          string[] v = lin.Split(';');
//          if (v[0] != "CHRNR")
//          {
//            int tal = int.Parse(v[0]);
//            if (tal < 10000 || tal >= 900000)
//            { 
//              // så er det et besætningsnummer, og så skal chr_nr findes i chr_samlet
//              tal = db.SingleSelect<int>("select chr_nr from chr_samlet where bes_nr= " + tal); 
//            }
//            // find posten i db.result
//            DataRow[] r = db.Result.Select("chr_nr=" + tal);
//            if (r.Length > 0)
//            {
//              r[0]["sus"] = v[3];
//              int skil = v[3].IndexOf("+");
//              if (skil>0)
//                r[0]["spf_status"] = v[3].Substring(0,skil);

//              // ??? vi har ikke fået en liste med prioritering af statusværdier, men ukendt og positiv må i hvert fald have forrang
//              if (r[0]["ap2"].ToString() == "6")
//              {
//                r[0]["myc"] = v[1];
//                r[0]["ap2"] = v[2];
//              }
//              else
//              {
//                //Debug.Print(tal.ToString() + ": " + r[0]["myc"].ToString() + " = " + v[1] + " - " + r[0]["ap2"].ToString() + " = " + v[2]);
//                // så skal myc og ap2 behandles hver for sig - 4 og 5 gør vi ikke noget ved
//                if (int.Parse(r[0]["myc"].ToString()) < 4 && int.Parse(v[1]) > 3)
//                {
//                  r[0]["myc"] = v[1];
//                }
//                if (int.Parse(r[0]["ap2"].ToString()) < 4 && int.Parse(v[2]) > 3)
//                {
//                  r[0]["ap2"] = v[2];
//                }
//              }
//              r[0].AcceptChanges(); 
//            }
//            //db.SetSQLStatement("insert into spf(chr_nr, myc, ap2) values(" + v[0] + "," + v[1] + "," + v[2] + ");");
//            //db.Execute(false);
				
//          }
//        }
//        //db.Result.AcceptChanges();
//        foreach (DataRow r in db.Result.Rows )
//        {
//          if (r["ap2"].ToString() != "6")
//          {
//            db.SetSQLStatement("update spf_svin set ap2='" + r["ap2"].ToString() + "', myc='" + r["myc"].ToString() + "', sus='" + r["sus"].ToString() + "', spf_status='" + r["spf_status"].ToString() + "' where chr_nr=" + r["chr_nr"].ToString());
//            db.Execute(false);
//          }
//        }
//      }
//      catch (Exception ex)
//      {
//        Console.Write(ex.Message);
//        db.RollbackTransaction();
//        db.Close();
//        throw new Exception("Insert af SPF-data fejlede!");
//      }
//      // skriv at importen er lykkedes i spf_opdatering

//      db.CommitTransaction();
//      db.Close();			
			
//    }
	}
}
